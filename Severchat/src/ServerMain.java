import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServerMain {
    static ArrayList<Socket> SO = new ArrayList<Socket>(10);
    static ArrayList<String> SN=new ArrayList<String>(10);

    static int ssr = 0;
    static BufferedReader fr;
    static PrintWriter fw;
    public static void main(String[] args) {
        int port = 8900;
        try {
            ServerSocket SSS = new ServerSocket(8900);
            while(true) {
                Socket S=SSS.accept();
                ssr++;
                SO.add(S);

                System.out.println(S);
                BufferedReader R = new BufferedReader(new InputStreamReader(S.getInputStream()));
                PrintWriter W = new PrintWriter(S.getOutputStream(), true);
                Cre_Log T = new Cre_Log(S);
                T.start();
            }

        } catch (IOException ex) {
            Logger.getLogger(ServerMain.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}


class Cre_Log extends Thread {

    Socket S;

    public Cre_Log(Socket S) {
        this.S = S;
    }

    @Override
    public void run() {

        try {
            BufferedReader R = new BufferedReader(new InputStreamReader(S.getInputStream()));
            PrintWriter W = new PrintWriter(S.getOutputStream(), true);



            rd_wr rw = new rd_wr(S);
            rw.start();

        } catch (IOException ex) {
            Logger.getLogger(Cre_Log.class.getName()).log(Level.SEVERE, null, ex);

        }

    }
}


class rd_wr extends Thread {

    Socket s;
    String name;

    public rd_wr(Socket s) {
        this.s = s;

    }

    @Override
    public void run() {
        try {
            //To change body of generated methods, choose Tools | Templates.
            PrintWriter out = new PrintWriter(s.getOutputStream(), true);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(s.getInputStream())
            );
            String line;
            while((line=in.readLine())!=null){

                String msg = checkstring(line) ;
                out.println(msg);

            }


        } catch (IOException ex) {
            Logger.getLogger(rd_wr.class.getName()).log(Level.SEVERE, null, ex);
        }



    }

    public String checkstring(String line){
        if(isStringUpperCase(line)==true){
            return line.toLowerCase();
        }
        else {
            return line.toUpperCase();
        }
    }
    private static boolean isStringUpperCase(String str){

        //convert String to char array
        char[] charArray = str.toCharArray();

        for(int i=0; i < charArray.length; i++){

            //if the character is a letter
            if( Character.isLetter(charArray[i]) ){

                //if any character is not in upper case, return false
                if( !Character.isUpperCase( charArray[i] ))
                    return false;
            }
        }

        return true;
    }


}


